<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'title' => $faker->realText($maxNbChars=10, $indexSize=2),
        'author_name' => $faker->name,
        'price' => $faker -> randomFloat(3, 0, 1000),
        'admin_id' => 1,
    ];
});
