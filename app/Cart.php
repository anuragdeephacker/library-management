<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Cart
{
    public $items = array();
    public $totalQty = 0;
    public $totalPrice = 0;

    public function __construct($oldCart)
    {
        if ($oldCart) {

            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    public function add($item, $id)
    {
        if(in_array($id, $this->items)==false)
        {
            array_push($this->items, $id);
            $this->totalQty++;
            $this->totalPrice += Book::find($id)->price;
        }

        // print_r($this->items);
        // print($this->totalPrice);
        // print($this->totalQty);

    }
}
