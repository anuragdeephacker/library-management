<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Cart;
use Session;
use App\User;

class CartController extends Controller
{
    private $cart;
    public function index()
    {
        $books = Book::all();
        return view('user.index', compact('books'));
    }

    public function storeToCart($book)
    {
        $book = Book::find($book);

        $session = request()->session();
        $oldCart = $session->has('cart') ? $session->get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($book, $book->id);
        request()->session()->put('cart', $cart);


        return redirect('/home');
    }

    public function store()
    {
        if (auth()->user() == null) {
            return redirect('/login');
        }
        $cart = session()->get('cart');

        $user = auth()->user();

        $store = array();

        foreach ($cart->items as $item) {
            $book = Book::find($item);
            $price = $book->price;
            $d = 0;
            $store[$item] = [
                'past_charges' => $d, 
                'current_charge' => $price,
            ];
        }
        $user->hasRented()->sync($store);
        $user = User::find($user->id);
        dd($user->hasRented->find($cart->items[0])->pivot);
    }
}
