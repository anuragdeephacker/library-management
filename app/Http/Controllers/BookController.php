<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Book;

class BookController extends Controller
{
    protected function __constructor()
    {
        $this->middleware('auth:admin');
    }

    protected function guard()
    {
        if (Auth::guard('admin')->check()) return 'admin';
        else return 'web';
    }

    private function checkGuard()
    {
        $guard = $this->guard();

        switch ($guard) {
            case 'admin':
                if (Auth::guard($guard)->check()) {
                    return true;
                }
                break;

            default:
                if (Auth::guard($guard)->check()) {
                    return redirect('/home');
                }
                break;
        }
    }

    public function create()
    {
        $this->checkGuard();
        $user = auth()->user();
        return view('admin.create', compact('user'));
    }

    public function index()
    {
        $this->checkGuard();
        $user = auth()->user();
        $books = Book::all();
        // dd($books);
        return view('admin.index', compact('user','books'));
    }

    public function store()
    {
        $this->checkGuard();
        $data = $this->validate(request(), [
            'title' => 'required',
            'author_name' => 'required',
            'price' => 'required|numeric',
        ]);

        // dd(auth()->user()->id);
        $data = array_merge($data, ['admin_id'=> auth()->user()->id ]);
        // dd($data);
        $book  = Book::create($data);
        // $book->title = $data->title;
        // $book->author_name = $data->author_name;
        // $book->price = $data->price;
        // $book->push();
        echo "book created";
        return redirect('/admin/books');

    }
    public function show(Book $book)
    {
        return view('admin.show', compact('book'));
    }
    public function edit(Book $book)
    {
        $this->checkguard();
        $user = auth()->user();

        return view('admin.edit', compact('book'));
    }
    public function update(Book $book)
    {
        $this->checkGuard();

        $data = $this->validate(request(), [
            'title' => 'required',
            'author_name' => 'required',
            'price' => 'required|numeric',
        ]);
        // dd($data);
        $book->update($data);
        return redirect('/admin/books');


    }
    public function destroy(Book $book)
    {
        $book->delete();
        // dump("item deleted!");
        return redirect('/admin/books');
    }
}
