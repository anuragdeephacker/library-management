<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $guard = 'admin';

    protected $fillable = [
        'title', 'author_name', 'price', 'admin_id'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function rentedBy()
    {
        return $this->belongsToMany('App\User')->withPivot('past_charges', 'current_charge')->withTimestamps();
    }
}
