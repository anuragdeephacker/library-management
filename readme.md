<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"><h1>By Anurag Deep<h1></p>

<p align="center">
</p>


## About

Database: MySql (5.7+)
Laravel: 5.8
Php : 7.2+


Its a library management system.

Actors:
1. Admin
2. Student

Giving model idea:
1. There will be a User model whose instance will represent the actors in our system(Admin, Student)
2. There will a Book model whose description will get from below docs.
3. User and Book model will have one to many relationship.
4. We will be using polymorphic relation on the User model to get the Admin and Student model instances.
5. Student and Admin model respectively.
6. Account model where the Student charges would be saved.

Use cases:
1. Admin can login(admin details will be populated from the seeder file)
2. Admin can add new Books, delete Books and update Books.
3. Both soft deletion and Hard deletion should be there as of admin choice.
4. Admin will decide the price of each Book (each price would be assumed to be a weekly price)
5. Student can login/signup (no need of email verification)
6. Student can choose Books he want to get issued from the library. He can add to his collection.
7. At the end of every week(Sunday) a job would run which would calculate the charges associated with the Student and will be updated in the Account
8. Admin can view the charges of each Student over each Book.
9. Every week  a report of the Student charges has to be printed in a log file through Laravel Queues.


Key points:
1. Session driver has to be file system
2. Queue driver has to be database.
3. You can use laravel authentication for login procedures
4. All data handling has to be done from the APIs
5. APIs will be protected by laravel/passport
6. No need of any css. Just plain formatted html would be sufficient.
7. It would be great if you can make this as Single Page Application though it is not a mandatory thing.

