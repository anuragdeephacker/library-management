<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
// use Illuminate\Routing\Route;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
// all the cart routes 
Route::get('/home', 'CartController@index' );
Route::post('/cart/store', 'CartController@store');
Route::post('/cart/{book}/add', 'CartController@storeToCart')->middleware('web');
Route::post('/users/logout', 'Auth\LoginController@userLogout')->name('user.logout');


// Route::match(['GET','POST'],'add-cart','CartController@addtoCart');

Route::prefix('admin')->group(function() {
  Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
  Route::get('/', 'AdminController@index')->name('admin.dashboard');
  // Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

  Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
  // routes for books crud;
  Route::post('/books', 'BookController@store');
  Route::get('/books/create', 'BookController@create'); 
  Route::get('/books', 'BookController@index');
  Route::get('/books/{book}', 'BookController@show');
  Route::get('/books/{book}/edit', 'BookController@edit');
  Route::put('/books/{book}', 'BookController@update');
  Route::delete('/books/{book}', 'BookController@destroy');
});


