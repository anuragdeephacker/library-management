@extends('layouts.user-app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">User Dashboard</div>
                <div class="panel-heading"><h4>Total Price : {{ session()->get('cart')->totalPrice ?? 0}} __  
                    Total Quantity : {{ session()->get('cart')->totalQty ?? 0}}</h4>
                </div>
                <div class="panel-heading">
                    <form action="/cart/store" method="POST">
                        @csrf
                    <button type="submit" class="btn btn-success">Checkout</button>
                    </form>
                </div>   
                <div class="panel-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Title</th>
                                <th scope="col">Author Name</th>
                                <th scope="col">Price</th>
                                <th scope="col">User Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($books as $book)
                            <tr>
                                <td>{{$book->id}}- {{$book->title}}</td>
                                <td>{{$book->author_name}}</td>
                                <td>{{$book->price}}</td>
                                <td>
                                    <form action="/cart/{{$book->id}}/add" method="POST"> 
                                    @csrf
                                    <button type="submit" class="btn btn-primary">Add to wishlist</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                </div>

            </div>
        </div>
    </div>
</div>
@endsection