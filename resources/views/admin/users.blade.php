@extends('layouts.user-app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">USER Dashboard</div>
                <div class="container">
                    <div class="row">
                        <div class="col-3">
                            <button type="button" class="btn btn-primary btn-dark">SHOW BOOKS</button>
                            <button type="button" class="btn btn-primary btn-primary">SHOW USERS</button>
                            <!-- <button type="button" class="btn btn-primary btn-dark btn-block">SHOW ACCOUNTS</button> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-9">
                            <h1>USERS</h1>
                            <table class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">USER NAME</th>
                                            <th scope="col">ID</th>
                                            <th scope="col">Total Charges</th>
                                            <th scope="col">Admin Actions on User</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>23</td>
                                            <td>
                                                <button>edit</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>66</td>
                                            <td>
                                                <button>edit</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Larry</td>
                                            <td>the Bird</td>
                                            <td>77</td>
                                            <td>
                                                <button>edit</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection