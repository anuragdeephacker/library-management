@extends('layouts.admin-app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3">
            <button type="button" class="btn btn-primary btn-primary active">SHOW BOOKS : CREATE</button>
            <button type="button" class="btn btn-primary btn-dark">SHOW USERS</button>
            <!-- <button type="button" class="btn btn-primary btn-dark btn-block">SHOW ACCOUNTS</button> -->
        </div>
    </div>
    <div class="row">
        <div class="col-9">
            <h1>CREATE BOOKS</h1>
            <form action="/admin/books" method="POST">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="title">Book Title</label>
                    <input type="text" class="form-control" id="title" name="title">
                </div>

                <div class="form-group">
                    <label for="author_name">Author Name</label>
                    <input type="text" class="form-control" id="author_name" name="author_name">
                </div>

                <div class="form-group">
                    <label for="price">Price</label>
                    <input type="number" class="form-control" id="price" name="price">
                </div>

                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
</div>





@endsection