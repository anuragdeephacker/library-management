@extends('layouts.admin-app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3">
            <button type="button" class="btn btn-primary btn-primary active">SHOW BOOKS</button>
            <button type="button" class="btn btn-primary btn-dark">SHOW USERS</button>
            <!-- <button type="button" class="btn btn-primary btn-dark btn-block">SHOW ACCOUNTS</button> -->
        </div>
    </div>
    <div class="row">
        <div class="col-9">
            <h1>Admin Dashboard</h1>
            <p>book list</p>
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Title</th>
                        <th scope="col">Author Name</th>
                        <th scope="col">Price</th>
                        <th scope="col">Admin Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($books as $book)
                    <tr>
                        <td>{{$book->id}}</td>
                        <td>{{$book->title}}</td>
                        <td>{{$book->author_name}}</td>
                        <td>{{$book->price}}</td>
                        <td class="flex">
                            <form action="/admin/books/{{ $book->id }}/edit">
                                <button type="submit" class="btn btn-default">EDIT</button>
                            </form>
                            <form action="/admin/books/{{ $book->id }}" method="POST">
                                @csrf
                                @method('DELETE');
                                <button type="submit" class="btn btn-default">DELEte</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>

        </div>
    </div>
</div>
    @endsection