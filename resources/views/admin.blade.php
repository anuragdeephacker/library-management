@extends('layouts.admin-app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"> Admin Dashboard</div>

                <div class="panel-body">
                    @component('components.who')

                    @endcomponent
                </div>

                <div class="panel-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-3">
                                <form action="/books">
                                    <button type="submit" class="btn btn-default">SHOW BOOKS</button>
                                </form>
                                <form action="/users">
                                    <button type="submit" class="btn btn-default">SHOW USERS</button>
                                </form>
                                <!-- <a href="/books"><button type="button" class="btn btn-info">SHOW BOOKS</button></a> -->
                                <!-- <a href="/users"><button type="button" class="btn btn-primary">SHOW USERS</button></a> -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-9">
                                <h1>Admin Dashboard</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection